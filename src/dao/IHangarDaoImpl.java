package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Hangars;

public class IHangarDaoImpl implements IHangarDao {

	@Override
	public void insertHangars(Hangars hangars) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(hangars);
		tx.commit();

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Hangars> view() {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		List<Hangars> list = new ArrayList<Hangars>();
		Query q = session.createQuery("from Hangars");
		List<Hangars> l = q.list();
		for (Hangars hangars : l) {
			list.add(hangars);
		}
		return list;
	}

	@Override
	public void updateHangars(Hangars hangars) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("update Hangars set hname=:hname,size=:size,location=:location where hid=:hid");
		q.setParameter("hname", hangars.getHname());
		q.setParameter("size", hangars.getSize());
		q.setParameter("location", hangars.getLocation());
		q.setParameter("hid", hangars.getHid());
		q.executeUpdate();
		session.save(hangars);

	}

}
