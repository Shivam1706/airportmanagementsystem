package dao;

import java.util.List;

import model.Hangars;

public interface IHangarDao {
	public void insertHangars(Hangars hangars);

	public List<Hangars> view();

	public void updateHangars(Hangars hangars);

}
