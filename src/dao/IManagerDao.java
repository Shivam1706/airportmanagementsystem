package dao;

import model.AirportManager;

public interface IManagerDao {
	public boolean managerAuthentication(AirportManager m);

	public void insertManager(AirportManager m);

}
