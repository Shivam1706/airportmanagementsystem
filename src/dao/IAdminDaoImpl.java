package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.AirportAdmin;

public class IAdminDaoImpl implements IAdminDao {

	@Override
	public void insertAdmin(AirportAdmin admin) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(admin);
		tx.commit();

	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public boolean adminAuthentication(AirportAdmin admin) {
		boolean isValid = false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("from AirportAdmin where vendorId=?1 and password=?2");
		q.setParameter(1, admin.getVendorId());
		q.setParameter(2, admin.getPassword());
		List list = q.list();
		if (list != null && list.size() > 0) {
			isValid = true;
		}

		return isValid;
	}

}
