package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Pilots;

public class IPilotDaoImpl implements IPilotDao {

	@Override
	public void insertPilots(Pilots pilots) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(pilots);
		tx.commit();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Pilots> view() {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		List<Pilots> list = new ArrayList<Pilots>();
		Query q = session.createQuery("from Pilots");
		List<Pilots> l = q.list();
		for (Pilots pilots : l) {
			list.add(pilots);
		}

		return list;
	}

	@Override
	public void updatePilots(Pilots pilots) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("update Pilots set age=:age,mobile=:mobile where id=:id");
		q.setParameter("age", pilots.getAge());
		q.setParameter("mobile", pilots.getMobile());
		q.setParameter("id", pilots.getId());
		q.executeUpdate();
		session.save(pilots);

	}

}
