package dao;

import model.AirportAdmin;

public interface IAdminDao {

	public boolean adminAuthentication(AirportAdmin admin);

	public void insertAdmin(AirportAdmin admin);

}
