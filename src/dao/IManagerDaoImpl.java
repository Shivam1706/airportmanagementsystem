package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.AirportManager;

public class IManagerDaoImpl implements IManagerDao {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean managerAuthentication(AirportManager m) {
		boolean isValid = false;
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();

		session.beginTransaction();

		Query q = session.createQuery("from AirportManager where managerId=?1 and password=?2");
		q.setParameter(1, m.getManagerId());
		q.setParameter(2, m.getPassword());

		List list = q.list();
		if (list != null && list.size() > 0) {
			isValid = true;
		}

		return isValid;
	}

	@Override
	public void insertManager(AirportManager m) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(m);
		tx.commit();

	}
}
