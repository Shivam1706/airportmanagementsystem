package dao;

import java.util.List;

import model.Planes;

public interface IPlaneDao {
	public void insertPlanes(Planes planes);

	public List<Planes> view();

	public void updatePlanes(Planes planes);

}
