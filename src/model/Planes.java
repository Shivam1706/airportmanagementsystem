package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Planes {
	@Id
	private Integer pid;
	private String pname;
	private String source;
	private String destination;
	private Integer capacity;

	public Planes() {
		// TODO Auto-generated constructor stub
	}

	public Planes(Integer pid, String pname, String source, String destination, Integer capacity) {
		super();
		this.pid = pid;
		this.pname = pname;
		this.source = source;
		this.destination = destination;
		this.capacity = capacity;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

}
