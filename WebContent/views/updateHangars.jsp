<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Update Hangars</h1>
<a href="display">Back</a>
<s:form action="update" method="post" modelAttribute="u">
Hangar name<s:input path="hname" autocomplete="off"></s:input><br/>
Size<s:input path="size" autocomplete="off"></s:input><br/>
Location<s:input path="location" autocomplete="off"></s:input><br/>
Hangar Id<s:input path="hid" autocomplete="off"></s:input><br/>
<s:button>Update</s:button>
</s:form>

</body>
</html>
