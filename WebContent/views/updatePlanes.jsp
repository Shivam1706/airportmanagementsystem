<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Planes</title>
</head>
<body>
<h1>Update Planes</h1>
<a href="display">Back</a>
<s:form action="update" method="post" modelAttribute="u">
Source<s:input path="source" autocomplete="off"></s:input><br/>
Destination<s:input path="destination" autocomplete="off"></s:input><br/>
Plane Id<s:input path="pid" autocomplete="off"></s:input><br/>
<s:button>Update</s:button>

</s:form>




</body>
</html>
