<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Hangar Details</title>
</head>
<body>
<a href="index.jsp">Home</a>
<br/><br/>
<table border="5">
<tr>
<th colspan="4">Pilot Details<br/></th></tr>
<tr>
<th>Hangar Id</th>
<th>Hangar Name</th>
<th>Size</th>
<th>Location</th>
</tr>
<c:forEach items="${h}" var="ha">
<tr style="text-align:center;">
<td>${ha.getHid()}</td>
<td>${pi.getHname()}</td>
<td>${pi.getSize()}</td>
<td>${pi.getLocation()}</td>
<td><a style="text-decoration:none" href="updateHangars">Update</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>
