<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login</title>
</head>
<body>
<h1>Welcome to Admin Login</h1>
<h4>${v}</h4>
<a href="index">Home</a><br/>
<s:form action="adminValidate" method="post" modelAttribute="a">
<table>
<tr><td><label for="vendorId">User Name</label></td>
<td><input id="vendorId" type="text" name="vendorId" autocomplete="off"/></td></tr>
<tr><td><label for="password">Password</label></td>
<td><input id="password" type="password" name="password"/></td></tr>
<tr><td>
<input type="submit" value="Login"/></td></tr>
</table>
</s:form>
</body>
</html>
