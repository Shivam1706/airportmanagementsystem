<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Register</title>
</head>
<body>
<h1>Admin Registration</h1><hr/>
<a href="index.jsp">Home</a><br/><br/>
<s:form action="adminAdd" method="post" modelAttribute="a">
First Name<s:input path="firstName" autocomplete="off"></s:input><br/>
Last Name<s:input path="lastName" autocomplete="off"></s:input><br/>
Age<s:input path="age" autocomplete="off"></s:input><br/>
Gender<s:input path="gender" autocomplete="off"></s:input><br/>
Contact Number<s:input path="contactNumber" autocomplete="off"></s:input><br/>
Vendor Id<s:input path="vendorId" autocomplete="off"></s:input><br/>
Password<s:input path="password"></s:input><br/>
<s:button>Register</s:button>
</s:form>
</body>
</html>
