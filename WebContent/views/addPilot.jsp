<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Pilots</title>
</head>
<body>
<h1>Add Pilots</h1>
<a href="index">Home</a><br/><br/>
<s:form action="insert" method="post" modelAttribute="pi">
Pilot Id<s:input path="id" autocomplete="off"></s:input><br/>
Pilot Name<s:input path="name" autocomplete="off"></s:input><br/>
Age<s:input path="age" autocomplete="off"></s:input><br/>
Gender<s:input path="gender" autocomplete="off"></s:input><br/>
Mobile<s:input path="mobile" autocomplete="off"></s:input><br/>
<s:button>Add</s:button>
</s:form>
</body>
</html>
