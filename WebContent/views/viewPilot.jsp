<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Pilot Details</title>
</head>
<body>
<a href="index.jsp">Home</a>
<br/><br/>
<table border="5">
<tr>
<th colspan="5">Pilot Details<br/></th></tr>
<tr>
<th>Pilot Id</th>
<th>Pilot Name</th>
<th>Age</th>
<th>Gender</th>
<th>Mobile</th>
</tr>
<c:forEach items="${pi}" var="pi">
<tr style="text-align:center;">
<td>${pi.getId()}</td>
<td>${pi.getName()}</td>
<td>${pi.getAge()}</td>
<td>${pi.getGender()}</td>
<td>${pi.getMobile()}</td>
<td><a style="text-decoration:none" href="updatePilot">Update</a></td>
</tr>
</c:forEach>
</table>

</body>
</html>
